# Makefile

CC=g++
CXXFLAGS= -W -Wall -ansi -pedantic -g -std=c++17
EXEC=main
SRC= $(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)
 
all: $(EXEC)
 
main: $(OBJ)
	$(CC) -o $@  $^ 
 
main.o: main.cpp vector3.hh camera.hh object.hh noise.hh test.hxx
	$(CC) -o $@ -c $< $(CXXFLAGS)
 
clean:
	rm -rf *.o
	rm -rf main