#include "light.hh"
#include "object.hh"

Light::Light(Vector3 intensity, Vector3 pos):
i_l(intensity), pos(pos)
{}

Vector3 Light::get_intensity(){
    return i_l;
}
Vector3 Light::get_pos(){
    return pos;
}
