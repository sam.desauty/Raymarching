#include "vector3.hh"
#include "camera.hh"
#include "object.hh"
#include "test.hxx"
#include "light.hh"
#include "scene.hh"

#include <cassert>
#include <vector>

/************* colors init ***************/

Vector3 blue(0, 0, 150);
Vector3 black(70, 0, 70);
Vector3 purple(194, 139, 255);
Vector3 anis(166, 208, 148);
Vector3 lightblue(0, 255, 255);
Vector3 yellow(255, 255, 0);
Vector3 red(255, 0, 0);
Vector3 darkblue(0, 0, 255);
Vector3 pink(239,124,243);
Vector3 skyblue(219, 251, 255);
Vector3 forestgreen(0,20,0);
Vector3 brown(25, 10, 0);
Vector3 peach(255, 203, 164);
Vector3 lightstone(50, 50, 50);
Vector3 stone(30, 30, 30);

/****************************************/


vector<Light> lights()
{
	Light l2(Vector3(255, 255, 255), Vector3(3, 4, 10));
	return {l2};
}

void log(int i, int total)
{
	if (i == total/4)
		cout << "	processed 25%.." << endl;
	else if (i == total/2)
		cout << "	processed 50%.." << endl;
	else if (i == total/4 + total/2)
		cout << "	processed 75%.." << endl;
}

int main(int argc, char* argv[]){
	if (argc < 2){
		cout << "usage: ./main outfile" << endl;
		return 1;
	}

	test::run_tests();

    /********** parameters init **************/

	Vector3 Campos(0, 0, 0);
	Vector3 watchpoint(0,0,1);
	Vector3 top(0,1,0);
	float fov = 45;
	int height = 480 ;
    int width = 720;
	Camera cam = Camera(Campos, watchpoint, top, fov);

	Vector3 lightcolor(255, 255, 255);
	Light l1(lightcolor, Vector3(0, 1, 0));
	Light l2(lightcolor, Vector3(3, 6, 10));

	Uniform_Texture text(0.7, 0.45);
	Ellipsoid e2(Vector3(-4, -2, 30), Vector3(4, 6, 2), text, anis);
	Ellipsoid e3(Vector3(-1, -2, 30), Vector3(4, 8, 2), text, anis);
	Sphere s(Vector3(-2, -3, 15), 3, Uniform_Texture(3, 0.3), red);
	Composite mount({&e2, &e3}, "mountain", Uniform_Texture(0, 1), Vector3(50, 50, 50));
	Composite bush({&s}, "bush", Uniform_Texture(0, 1), forestgreen);

	Plane sky(Vector3(0, 10, 0), Vector3(0, 1, 0), text, Vector3(51, 153, 255));
	
	Uniform_Texture lake_text(1, 1);
	lake_text.reflexion = 0;
	Ground lake(-2, "water", 0.3, lake_text, Vector3(15, 15, 50));

	Vector3 stone(18, 20, 20);
	Vector3 lightstone(28, 30, 30);

	Cone c1(Vector3(-18, 6, 32), 35, text, stone);
	Cone c2(Vector3(-12, 7, 35), 40, text, stone);
	Cone c3(Vector3(-11, 4, 30), 50, text, stone);
	Cone c4(Vector3(-8, 7, 35), 35, text, stone);
	Cone c5(Vector3(1, 7, 35), 50, text, stone);
	Cone c6(Vector3(5, 4, 30), 40, text, stone);
	Cone c78(Vector3(9, 4, 30), 50, text, lightstone);
	Cone c9(Vector3(14, 5, 35), 35, text, stone);
	Cone c10(Vector3(15, 2, 30), 40, text, lightstone);
	Cone fond1(Vector3(-18, 10, 45), 50, text, lightstone);
	Cone fond2(Vector3(-5, 10, 45), 50, text, lightstone);
	Cone fond3(Vector3(-10, 10, 45), 30, text, lightstone);
	Cone fond4(Vector3(-20, 10, 45), 50, text, lightstone);
	Cone fond5(Vector3(7, 9, 45), 55, text, lightstone);
	Cone fond6(Vector3(18, 10, 45), 45, text, lightstone);
	Cone fond7(Vector3(24, 7, 40), 25, text, lightstone);

	Composite mount1({&c1}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount2({&c2}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount3({&c3}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount4({&c4}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount5({&c5}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount6({&c6}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount78({&c78}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount9({&c9}, "mountain", Uniform_Texture(0, 1), stone);
	Composite mount10({&c10}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount11({&fond1}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount12({&fond2}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount13({&fond3}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount14({&fond4}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount15({&fond5}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount16({&fond6}, "mountain", Uniform_Texture(0, 1), lightstone);
	Composite mount17({&fond7}, "mountain", Uniform_Texture(0, 1), lightstone);

	Cone c(Vector3(7, 0.2, 20), 75, text, brown);
	Cone cc(Vector3(-7, 0.1, 20), 70, text, brown);
	Cone ccc(Vector3(-2, -1.2, 5), 75, text, brown);
	Cone cccc(Vector3(2.5, -1.5, 4), 75, text, brown);

	Composite m({&c}, "circle_lake", Uniform_Texture(0, 1), brown);
	Composite mm({&cc}, "circle_lake", Uniform_Texture(0, 1), brown);
	Composite mmm({&ccc}, "circle_lake", Uniform_Texture(0, 1), brown);
	Composite mmmm({&cccc}, "circle_lake", Uniform_Texture(0, 1), brown);
					
    vector<Light> light = lights();

	// vector<Object*> objects = {&sand, &c1, &c2, &c4, &c5, &c6, &c8,&c9, &c10,
	//							  &c11, &sky, &fond1, &fond2, &fond3, &fond4, &fond5, &fond6, &fond7};
	// vector<Object*> objects = {&mount1, &mount2, &mount3, &mount4, &mount5, &mount6,
	// 						   &mount78, &mount9, &mount10, &mount11, &mount12, &mount14,
	// 						   &mount15, &mount16,  &mount17, &m, &mm, &mmm, &mmmm, &sky, &sand};

	vector<Object*> objects = {&lake, &sky, &mount11, &mount12, &mount13, &mount14, &mount15,
							  &mount16, &mount17, &mount1, &mount2, &mount3, &mount4, &mount5,
							  &mount6, &mount78, &mount9, &mount10, &m, &mm, &mmm, &mmmm};

	// Sphere sph(Vector3(1, 1, 10), 2, lake_text,Vector3(15, 15, 50));
	// vector<Object*> objects = {&lake, &cc, &ccc, &c, &cccc};

	/******************************************/

    cout << "definition of the scene..." << endl; 
	
	Scene scene(cam, objects, light, width, height);

    cout << "writing scene..." << endl;

	for (int j = 0; j < height; ++j)
    {
        for (int i = 0; i < width; ++i)
			scene.pixels[j*width + i] =  scene.compute_pixel(i, j);
		log(j, height);
	}
	
	//scene.smoth();

    cout << "saving scene..." << endl;
	scene.save_image(argv[1]);

	return 1;
}