#include "vector3.hh"
#include <cmath>
#include <algorithm>

Vector3::Vector3(float x, float y, float z):
    x(x), y(y), z(z) {}

ostream& operator<<(ostream& out, const Vector3& vect)
{
    out << "x= " << vect.x << " y= " << vect.y << " z= " << vect.z << endl;
    return out;
}

Vector3 operator * (const float &l, Vector3 &vec){
    return vec*l;
}

bool Vector3::is_same_size(const Vector3& a, const Vector3& b){
    return (a.x == b.x && a.y == b.y && a.z==b.z);
}

Vector3 Vector3::operator + (Vector3 const &other) const{
    return Vector3(x+other.x,y+other.y,z+other.z);
}
Vector3 Vector3::operator - (Vector3 const &other) const{
    return Vector3(x-other.x,y-other.y,z-other.z);
}

bool Vector3::operator == (Vector3 const &other) const{
    return other.x == x && other.y == y && other.z == z;
}

Vector3 Vector3::operator * (const float &l) const{
    return Vector3(l*x, l*y, l*z);
}

 Vector3 Vector3::operator * (Vector3 const &other) const{
     return Vector3(x*other.x, y*other.y, z*other.z);
 }

Vector3 Vector3::operator *= (const float &l) {
    x*=l;
    y*=y;
    z*=l;
    return *this;
}

Vector3 Vector3::operator / (const float &l) const{
    return Vector3(x/l, y/l, z/l);
}

Vector3 Vector3::operator / (Vector3 const &l) const{
    return Vector3(x/l.x, y/l.y, z/l.z);
}

float Vector3::distance() const{
    return sqrt(x*x + y*y + z*z);
}

Vector3  Vector3::normalize() const{
    if (*this == Vector3(0, 0, 0))
        return *this;
    return Vector3(*this/this->distance());
}

Vector3 Vector3::cross(Vector3 other) const{
    return Vector3(y * other.z - z * other.get_y(),
                    z * other.x - x * other.z,
                    x * other.y - y * other.x);
}

float Vector3::dot(Vector3 other) const{
    return x*other.x + y*other.y + z*other.z;
}

Vector3 Vector3::thresh() const {
    return Vector3(min(x, 255.f), min(y, 255.f), min(z, 255.f));
}

float Vector3::get_x(){return x;}
float Vector3::get_y(){return y;}
float Vector3::get_z(){return z;}

Vector3 abs(Vector3 vec){
    return Vector3(abs(vec.get_x()), abs(vec.get_y()), abs(vec.get_z()));
}

Vector3 max(Vector3 a, Vector3 b)
{
    return Vector3(fmaxf(a.get_x(), b.get_x()), fmaxf(a.get_y(), b.get_y()), fmaxf(a.get_z(), b.get_z()));
}

Vector3 max(Vector3 a, float b)
{
    return Vector3(fmaxf(a.get_x(), b), fmaxf(a.get_y(), b), fmaxf(a.get_z(), b));
}
