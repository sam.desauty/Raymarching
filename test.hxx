#pragma once
#include <cassert>
#include "vector3.hh"
#include "camera.hh"
#include "object.hh"

namespace test{
    void test_camera(){
        Vector3 pos(0,0,0);
        Vector3 watch_point(0,0,1);
        Vector3 top(0,1,0);
        float fov=45;
        Camera cam(pos, watch_point, top, fov);

        assert((cam.get_right_vec() == Vector3(1,0,0)));
        assert((cam.get_third_dim() == Vector3(0,0,1)));
        assert((cam.get_distance() == 1));
        assert((Vector3(100, 435, 155).thresh() == Vector3(100, 255, 155)));
        assert ((Vector3(3000, 435, 155).thresh() == Vector3(255, 255, 155)));
        assert ((Vector3(3000, 50, 155).thresh() == Vector3(255, 50, 155)));

        cout << "[PASSES] test_camera" << endl;
    }

    void test_vectors(){
        Vector3 vec1(1,1,1);
        Vector3 vec2(2,2,2);
        Vector3 vec3 (1, 2, 2);

        assert(2.f*vec1 == vec2);
        assert(vec1 == vec1);
        assert(vec3.distance() == 3);
        assert(vec2.dot(vec1) == 6);
        assert(vec2.cross(vec1) == Vector3(0,0,0));

        cout << "[PASSES] test_vectors" << endl;
    }

    void test_objects(){
        Vector3 og(0,0,0);
        Vector3 dir(0,0,1);
        Ray r(og, dir);

        assert(r.point_at(2) == Vector3(0,0,2));


        Sphere s(og, 1, Uniform_Texture(0,0), Vector3(0,0,0));

        cout << "[PASSES] test_objects" << endl;
    }

    void run_tests(){
        test_camera();	
        test_vectors();
        test_objects();
    }
}
