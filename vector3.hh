#ifndef VECTOR3_HH
#define VECTOR3_HH

// #include <vector>
#include <iostream>

using namespace std;


class Vector3{
    public:
        Vector3(float x=0, float y=0, float z=0);

        bool is_same_size(const Vector3& a, const Vector3& b);

        Vector3 operator + (Vector3 const &other) const;

        Vector3 operator - (Vector3 const &other) const;

        bool operator == (Vector3 const &other) const;

        Vector3 operator * (const float &l) const;

        Vector3 operator * (Vector3 const &other) const;

        Vector3 operator *= (const float &l);

        Vector3 operator / (const float &l) const;
        Vector3 operator / (Vector3 const &l) const;

        float distance() const;
        
        Vector3  normalize() const;

        Vector3 cross(Vector3 other) const;

        float dot(Vector3 other) const;

        Vector3 thresh() const;

        friend ostream& operator<<(ostream& out, const Vector3& vect);
        friend Vector3 operator* (const float &l, Vector3 &vec);

        float get_x();
        float get_y();
        float get_z();

    private:
        float x;
        float y;
        float z;
};

Vector3 abs(Vector3 vec);
Vector3 max(Vector3 a, Vector3 b);
Vector3 max(Vector3 a, float b);


#endif