#include "color.hh"

std::ostream& operator<<(std::ostream &out, Color c)
{
    out << c.r << ' ';
    out << c.g << ' ';
    out << c.b << ' ';
    return out;
}