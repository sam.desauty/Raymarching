#pragma once
#include <iostream>

struct Color
{
    Color(){}
    Color(float r, float g, float b)
    :r(r), g(g), b(b){}
    float r;
    float g;
    float b;
};

std::ostream& operator<<(std::ostream &out, Color c);