#include "object.hh"
#include "noise.hh"

// Texture_Material::Texture_Material(float k_s, float k_d):
// k_d(k_d), k_s(k_s)
// {}

Uniform_Texture::Uniform_Texture(float k_s, float k_d): //Texture_Material(k_s, k_d)
k_d(k_d), k_s(k_s), reflexion(0) {}

float Uniform_Texture::get_k_d(){
    return k_d;
}
float Uniform_Texture:: get_k_s(){
    return k_s;
}

/*************************************************/

Ray::Ray(Vector3 A, Vector3 B){
    Vector3 dir = (B - A)/(B-A).distance();
    this->direction = dir.normalize();
    this->begin = A;
}

Vector3 Ray::point_at(float t){
    return begin + t*direction;
}

Vector3 Ray::get_dir(){
    return direction;
}

Vector3 Ray::get_begin(){
    return begin;
}

/*************************************************/

Sphere::Sphere(Vector3 center, float radius, Uniform_Texture texture, Vector3 color):
 center(center), radius(radius), texture(texture), color(color){}

Vector3 Sphere::get_center(){
    return center;
}
    
float Sphere::get_radius(){
    return radius;
}

Vector3 Sphere::get_color(){
    return color;
}

float Sphere::SDF(Vector3 p){
    return (p-center).distance() - radius;
}


Uniform_Texture Sphere::get_texture(){
    return texture;
}

/*************************************************/


Plane::Plane(Vector3 point,Vector3 normal, Uniform_Texture texture, Vector3 color):
point(point), texture(texture), color(color), normal(normal){}

Vector3 Plane::get_color(){
    return color;
}

Vector3 Plane::get_normal(){
    return normal;
}

Uniform_Texture Plane::get_texture(){
    return texture;
}

float Plane::SDF(Vector3 p)
{
    if (normal == Vector3(0, 0, 1))
        return point.get_z() - p.get_z();
    if (normal == Vector3(0, 1, 0) && point.get_y() > 0)
        return point.get_y() - p.get_y();
    return p.dot(normal) - point.dot(normal);
}

Vector3 Plane::get_point(){
    return point;
}



/*************************************************/

Box::Box(Vector3 center, Vector3 size, Uniform_Texture texture, Vector3 color):
center(center), size(size), texture(texture), color(color){}

float Box::SDF(Vector3 p)
{
    Vector3 q = abs(p-center) - size;
    return max(q, 0.0f).distance() + min(max(q.get_x(),max(q.get_y(),q.get_z())),0.0f);
}

Vector3 Box::get_color(){
    return color;
}

Uniform_Texture Box::get_texture(){
    return texture;
}

Vector3 Box::get_size(){
    return size;
}

/*************************************************/

Cone::Cone(Vector3 center, float angle, Uniform_Texture texture, Vector3 color):
center(center), angle(angle), texture(texture), color(color){}

float Cone::SDF(Vector3 p)
{
    Vector3 temp =  p-center;
    Vector3 c(cos(angle* M_PI/180), sin(angle* M_PI/180), 0);
    float q = sqrt(temp.get_z()*temp.get_z() + temp.get_x()*temp.get_x());
    return c.dot(Vector3(q, temp.get_y(), 0));
}

Vector3 Cone::get_color(){
    return color;
}

Uniform_Texture Cone::get_texture(){
    return texture;
}

/*************************************************/

Ellipsoid::Ellipsoid(Vector3 center, Vector3 dim, Uniform_Texture texture, Vector3 color):
center(center), dim(dim), texture(texture), color(color){}

float Ellipsoid::SDF(Vector3 p)
{
    Vector3 po = p-center;
    float k0 = (po/dim).distance();
    float k1 = (po/(dim*dim)).distance();
    return k0*(k0-1.0)/k1;
}

Vector3 Ellipsoid::get_color(){
    return color;
}

Uniform_Texture Ellipsoid::get_texture(){
    return texture;
}

/*************************************************/

Torus::Torus(Vector3 center, float smallr, float bigr, Uniform_Texture texture, Vector3 color):
center(center), smallr(smallr), bigr(bigr), texture(texture), color(color){}

float Torus::SDF(Vector3 p)
{
    Vector3 po = p-center;
    float q1 = sqrt(po.get_x()*po.get_x() + po.get_y()*po.get_y()) - smallr;
    float len = sqrt(q1*q1 + po.get_z()*po.get_z());
    return len-bigr;
}

Vector3 Torus::get_color(){
    return color;
}

Uniform_Texture Torus::get_texture(){
    return texture;
}

/*************************************************/

Capsule::Capsule(Vector3 center, Vector3 orientation, float length, float radius, Uniform_Texture texture, Vector3 color):
center(center), orientation(orientation), length(length), radius(radius), texture(texture), color(color){}

float Capsule::SDF(Vector3 p)
{
    Vector3 po = p - center;
    // Vector3 tmp(po.get_x(), po.get_y()-clamp(po.get_y(), 0.0f, height), po.get_z());
    Vector3 tmp = po - orientation * clamp(orientation.dot(po), 0.0f, length);
    return tmp.distance() - radius;
}

Vector3 Capsule::get_color(){
    return color;
}

Uniform_Texture Capsule::get_texture(){
    return texture;
}

Vector3 Capsule::get_center()
{
    return center + orientation*length/2;
}

/*************************************************/

Ground::Ground(float pos, string type, float factor, Uniform_Texture texture, Vector3 color):
pos(pos), type(type), factor(factor), texture(texture), color(color){}

float Ground::SDF(Vector3 p)
{
    float displacement = 0;
    // if (type == "water")
    //     displacement = sinusoidal::noise(p.get_x(), p.get_y(), p.get_z(), 1.0, 0.15);
    // if (type == "waves")
    //     displacement = sin(p.get_x()*0.1)*sin(p.get_z()*factor);
    if (type == "water")
    {
        float fact = 0.1;
        displacement = perlin::OctavePerlin(fact*p.get_x(), fact*p.get_y(), fact*p.get_z(), 6, 0.3);
    }
    //     displacement = sin(p.get_x()*0.1)*sin(p.get_z()*factor);

    return p.get_y() - pos + displacement;
}

Vector3 Ground::get_color(){
    return color;
}

Uniform_Texture Ground::get_texture(){
    return texture;
}

/*************************************************/

Composite::Composite(vector<Object*> objects, string type,  Uniform_Texture texture, Vector3 color):
objects(objects), type(type), texture(texture), color(color){}

float Composite::SDF(Vector3 p)
{
    float dist = 0;
    if (type == "example")
    {
        float trois_cylindre = min(objects[0]->SDF(p), min(objects[1]->SDF(p), objects[2]->SDF(p)));
        float sphere_chelou = intersect(objects[3]->SDF(p), objects[4]->SDF(p));
        dist = substract(trois_cylindre, sphere_chelou);
    }
    else if (type == "bush")
    {
        int random_variable = (rand() % 10);
        dist = objects[0]->SDF(p) + random_variable*0.1;
    }
    else if (type == "mountain")
    {
        float fact = 0.5;
        float displacement = perlin::OctavePerlin(fact*p.get_x(), fact*p.get_y(), fact*p.get_z(), 5, 0.4);
        dist = objects[0]->SDF(p) + displacement;
    }
    else if (type == "circle_lake")
    {
        float fact = 0.2;
        float displacement = perlin::OctavePerlin(fact*p.get_x(), fact*p.get_y(), fact*p.get_z(), 5, 0.4);
        dist = objects[0]->SDF(p) + displacement;
    }
    else
        cout << "NOT A COMPOSITE TYPE !" << endl;

    return dist;
}
float Composite::unify(float a, float b, float k)
{
    // float res = exp2( -k*a ) + exp2( -k*b );
    // return -log2(res)/k;
    return min(a, b);
}

float Composite::substract(float d1, float d2){
    return max(-d1,d2);
}

float Composite::intersect(float d1, float d2 )
{
    return max(d1,d2);
}

Vector3 Composite::get_color(){
    return color;
}

Uniform_Texture Composite::get_texture(){
    return texture;
}
