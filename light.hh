#pragma once

#include "vector3.hh"
#include "object.hh"

class Light{
    public:
        Light(Vector3 intensity, Vector3 pos);

        Vector3 get_intensity();
        Vector3 get_pos();
    private:
        Vector3 i_l; //intensité lumineuse
        Vector3 pos;
};
