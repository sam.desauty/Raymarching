#include "noise.hh"

#include <cmath>

float sinusoidal::noise(float x, float y, float z, float factor, float attenuation){
        return sin(factor * x) * sin(factor * y) * sin(factor * z) * attenuation;
    }

float random::noise(float x, float y, float z, int range, float attenuation){
        return (rand() % range) * attenuation;
    }

double perlin::grad(int hash, double x, double y, double z) {
    int h = hash & 15;
    double u = h<8||h==12||h==13 ? x : y,
            v = h<4||h==12||h==13 ? y : z;
    return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

double perlin::fade(double t) {
    // Fade function as defined by Ken Perlin.
    return t * t * t * (t * (t * 6 - 15) + 10); // 6t^5 - 15t^4 + 10t^3
}

double perlin::lerp(double x, double a, double b) {
    return a + x * (b - a);
}
  
double perlin::noise(double x, double y, double z) {

    int p[512];
    for (int i = 0; i < 256 ; i++) {
        p[256+i] = p[i] = permutation[i];
    }

    int X = (int)floor(x) & 255;
    int Y = (int)floor(y) & 255;
    int Z = (int)floor(z) & 255;
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);

    double u = fade(x);
    double v = fade(y);
    double w = fade(z);

    int A = p[X] + Y;
    int AA = p[A] + Z;
    int AB = p[A + 1] + Z;
    int B = p[X + 1] + Y;
    int BA = p[B] + Z;
    int BB = p[B + 1] + Z;

    return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z),
                                   grad(p[BA], x - 1, y, z)),
                           lerp(u, grad(p[AB], x, y - 1, z),
                                   grad(p[BB], x - 1, y - 1, z))),
                   lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),
                                   grad(p[BA+1], x - 1, y, z - 1)),
                           lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
                                   grad(p[BB+1], x - 1, y - 1, z - 1))));
}

double perlin::OctavePerlin(double x, double y, double z, int octaves, double persistence) {
    double total = 0;
    double frequency = 1;
    double amplitude = 1;
    double maxValue = 0;			// Used for normalizing result to 0.0 - 1.0
    for(int i=0;i<octaves;i++) {
        total += noise(x * frequency, y * frequency, z * frequency) * amplitude;
        maxValue += amplitude;
        amplitude *= persistence;
        frequency *= 2;
    }
    return total/maxValue;
}
