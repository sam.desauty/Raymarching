#pragma once
#include <algorithm>
#include <cmath>
#include <vector>

#include "vector3.hh"

// class Texture_Material{
//     public:
//         Texture_Material(float k_s, float k_d);
//         virtual pair<float, float> get_texture(Vector3 position)=0;
//     private:
//         float k_d; //composante diffuse
//         float k_s; // composante speculaire
// };

class Uniform_Texture {
    public:
        Uniform_Texture(float k_s, float k_d);
        Uniform_Texture get_texture(Vector3 position); //override;
        float get_k_d();
        float get_k_s();
        float k_d; //composante diffuse
        float k_s; // composante speculaire
        int reflexion;
};

class Ray{
    public:
        Ray(Vector3 A, Vector3 B);

        Vector3 point_at(float t);
        Vector3 get_dir();
        Vector3 get_begin();

    private:
        Vector3 begin;
        Vector3 direction;
};

class Object{
    public:
        Object(){}
        virtual Vector3 get_color() = 0;
        virtual Uniform_Texture get_texture() = 0;
        virtual float SDF(Vector3 p) = 0;
        //virtual Vector3 normal(Vector3 point) = 0;
        virtual ~Object(){};
};

class Sphere: public Object{
    public:
        Sphere(Vector3 center, float radius, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_center();
        float get_radius();
        Uniform_Texture get_texture() override;
        Vector3 get_color() override;
        ~Sphere(){}

    private:
        Vector3 center;
        float radius;
        Uniform_Texture texture;
        Vector3 color;
};


class Plane: public Object{
    public:
        Plane(Vector3 point, Vector3 normal, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;
        Vector3 get_point();
        Vector3 get_normal();

    private:
        Vector3 point;
        Uniform_Texture texture;
        Vector3 color;
        Vector3 normal;
};


class Box: public Object{
    public:
        Box(Vector3 center, Vector3 size, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;
        Vector3 get_size();

    private:
        Vector3 center;
        Vector3 size; //width, height, depth
        Uniform_Texture texture;
        Vector3 color;
};

class Cone: public Object{
    public:
        Cone(Vector3 center, float angle, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;

    private:
        Vector3 center;
        float angle;
        Uniform_Texture texture;
        Vector3 color;

};

class Ellipsoid: public Object{
    public:
        Ellipsoid(Vector3 center, Vector3 dim, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;

    private:
        Vector3 center;
        Vector3 dim;
        Uniform_Texture texture;
        Vector3 color;

};

class Torus: public Object{
    public:
        Torus(Vector3 center, float smallr, float bigr, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;

    private:
        Vector3 center;
        float smallr;
        float bigr;
        Uniform_Texture texture;
        Vector3 color;

};

class Capsule: public Object{
    public:
        Capsule(Vector3 center, Vector3 orientation, float length, float radius, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;
        Vector3 get_center();

    private:
        Vector3 center;
        Vector3 orientation;
        float length;
        float radius;
        Uniform_Texture texture;
        Vector3 color;

};

class Ground: public Object{
    public:
        Ground(float pos, string type, float factor, Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;

    private:
        float pos;
        float factor;
        string type;
        Uniform_Texture texture;
        Vector3 color;
};

class Composite: public Object{
    public:
        Composite(vector<Object*> objects, string type,  Uniform_Texture texture, Vector3 color);

        float SDF(Vector3 p) override;
        Vector3 get_color() override;
        Uniform_Texture get_texture() override;
        float unify(float a, float b, float k=43);
        float substract(float d1, float d2);
        float intersect(float d1, float d2);

    private:
        vector<Object*> objects;
        string type;
        Uniform_Texture texture;
        Vector3 color;
};

