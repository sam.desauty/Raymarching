#include "scene.hh"
#include "light.hh"
#include "object.hh"
#include "noise.hh"

#include <fstream>  
#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <random>
#include <cstdlib>
// #include <experimental/random>



#define MAX_STEPS 80
#define MAX_DIST 100
#define SURF_DIST 0.015


Scene::Scene(Camera camera, vector<Object*> objects, vector<Light> lights,
             float width, float height) : camera(camera),
                                          objects(objects),
                                          lights(lights),
                                          width(width),
                                          height(height)
{
    this->pixels = vector<Vector3>(height * width, Vector3(0, 0, 0));
}

Camera Scene::get_camera()
{
    return camera;
}
vector<Light> Scene::get_lights()
{
    return lights;
}
float Scene::get_width()
{
    return width;
}
float Scene::get_height()
{
    return height;
}

void Scene::save_image(string filename)
{
    std::ofstream ofs(filename, ios::out | ios::binary);
    ofs << "P6\n"
        << width << " " << height << "\n255\n";
    for (int i = 0; i < width * height; ++i)
    {
        char r = (char)(pixels[i].get_x());
        char g = (char)(pixels[i].get_y());
        char b = (char)(pixels[i].get_z());
        ofs << r << g << b;
    }
    ofs.close();
}

Ray Scene::cast_ray(int i, int j)
{
    float fov = camera.get_fov();

    float imageAspectRatio = width / (float)height; // assuming width > height
    float Px = (2 * (i / width) - 1) * tan(fov / 2 * M_PI / 180) * imageAspectRatio;
    float Py = (1 - 2 * (j / height)) * tan(fov / 2 * M_PI / 180);
    Vector3 curpix(Px, Py, camera.get_watch_point().get_z());

    return Ray(camera.get_pos(), curpix);
}

float Scene::get_dist(Vector3 p)
{
    float dist = objects[0]->SDF(p);
    for (auto o : objects)
        dist = min(o->SDF(p),dist);
    return  dist;
}

float Scene::RayMarch(Vector3 ro, Vector3 dir)
{
    float depth = 0.;
    for (int i = 0; i < MAX_STEPS; ++i)
    {
        Vector3 p = ro + dir * depth;
        float dS = get_dist(p);
        depth += dS;
        if (dS < SURF_DIST || depth > MAX_DIST)
            return depth;
    }
    return depth;
}

Vector3 Scene::get_normal(Vector3 p)
{
    //compute gradiant

    float EPSILON = 0.001;

    float gradx = get_dist(Vector3(p.get_x() + EPSILON, p.get_y(), p.get_z())) - get_dist(Vector3(p.get_x() - EPSILON, p.get_y(), p.get_z()));

    float grady = get_dist(Vector3(p.get_x(), p.get_y() + EPSILON, p.get_z())) - get_dist(Vector3(p.get_x(), p.get_y() - EPSILON, p.get_z()));

    float gradz = get_dist(Vector3(p.get_x(), p.get_y(), p.get_z() + EPSILON)) - get_dist(Vector3(p.get_x(), p.get_y(), p.get_z() - EPSILON));

    return Vector3(gradx, grady, gradz).normalize();
}

pair<Vector3, Uniform_Texture> Scene::get_color(Vector3 point)
{
    for(auto o: objects)
    {
        if (o->SDF(point) <= SURF_DIST)
            return make_pair(o->get_color(), o->get_texture());
    }
    // return make_pair(Vector3(30, 30, 30),  Uniform_Texture(0, 1));
    if (point.get_z() > 20)
        return make_pair(Vector3(18, 20, 20),  Uniform_Texture(0, 1));
    return make_pair(Vector3(15, 5, 0),  Uniform_Texture(0, 1));
}

float smoothstep(float edge0, float edge1, float x) {
  x = clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.0f); 
  return x * x * (3 - 2 * x);
}

Vector3 compute_snow(Vector3 point)
{
    float contrast = 155;
    if (point.get_z() >=35)
        contrast = 255;
    if (point.get_y() > 5)
    {
        float fact = 0.4;
        float noise = 10*perlin::OctavePerlin(fact*point.get_x(), fact*point.get_y(), fact*point.get_z(), 6, 1);
        noise = contrast*(1-smoothstep(0, 1, noise)); //normalisation
        return Vector3(noise, noise, noise);
    }
    else if (point.get_y() > 4.5)
    {
        float fact = 0.3*point.get_y();
        float noise = 9*perlin::OctavePerlin(fact*point.get_x(), fact*point.get_y(), fact*point.get_z(), 6, 0.7);
        noise = contrast*(1-smoothstep(0, 1, noise)); //normalisation
        return Vector3(noise, noise, noise);
    }
    else if (point.get_y() > 3)
    {
        float fact = 0.2*point.get_y();
        float noise = 5*perlin::OctavePerlin(fact*point.get_x(), fact*point.get_y(), fact*point.get_z(), 6, 0.7);
        noise = contrast*(smoothstep(0, 1, noise)); //normalisation
        return Vector3(noise, noise, noise);
    }
    else
    {
        float noise = perlin::OctavePerlin(0.2*point.get_x(), 0.2*point.get_y(), 0.2*point.get_z(), 5, 0.5);
        noise = 255*smoothstep(0, 1, noise); //normalisation
        return Vector3(noise, noise, noise);
    }
}

Vector3 Scene::compute_illumination(Light l, Vector3 dir, Vector3 point, int count = 0)
{
    Vector3 N = get_normal(point);
    pair<Vector3, Uniform_Texture> coltex = get_color(point);

    const float shininess = 10.0;

    //rayon reflechi
    Vector3 S = (2 * (dir.dot(N)) * N - dir).normalize();
    Vector3 L = (l.get_pos() - point).normalize();

    float light_angle = N.dot(L) * M_PI / 180;

    Vector3 i_l = l.get_intensity();

    Vector3 diffuse = coltex.first * max(0.f, light_angle) * i_l * coltex.second.get_k_d();
    Vector3 specular = pow(S.dot(L), shininess) * i_l * coltex.second.get_k_s();

    Vector3 res = (diffuse + specular).thresh();
    
    if (coltex.first == Vector3(51, 153, 255) && !count) //sky
    {
        float noise = perlin::OctavePerlin(0.2*point.get_x(), 0.2*point.get_y(), 0.2*point.get_z(), 5, 0.5);
        noise = 255*smoothstep(0, 1, noise); //normalisation
        return (res + Vector3(noise, noise, noise)).thresh();
    }
    // else if ((coltex.first == Vector3(30, 30, 30) || coltex.first == Vector3(50, 50, 50) ) && point.get_y() > 3)
    else if ((coltex.first == Vector3(28, 30, 30) || coltex.first == Vector3(18, 20, 20)) && point.get_y() > 2)
        return (res + compute_snow(point)).thresh();
    else if (coltex.first == Vector3(25, 10, 0)) // grass
    {
        float fact = 0.4;
        float noise = 10*perlin::OctavePerlin(fact*point.get_x(), fact*point.get_y(), fact*point.get_z(), 6, 1);
        noise = 255*(1-smoothstep(0, 1, noise)); //normalisation
        return (Vector3(25, 10, 0) + Vector3(0,0.15*noise, 0.05*noise)).thresh();
    }
    //reflexion
    S = S + Vector3(0, 3, 10);
    float d = RayMarch(point, S);
    Ray ref(point, S);
    if (d < MAX_DIST && count != 1)
    {
        if (count) // reflexion
            res = (res + compute_illumination(l, ref.get_dir(), ref.point_at(d), count - 1)).thresh();
        else if (coltex.second.reflexion)
            res = (res + compute_illumination(l, ref.get_dir(), ref.point_at(d), 1)).thresh();
    }
    //Is Shadow
    d = RayMarch(point + N * SURF_DIST, L);
    if (d < (point - l.get_pos()).distance() && d > SURF_DIST)
        return Vector3(0, 0, 0);

    return res;
}

Vector3 Scene::compute_pixel(float i, float j)
{
    float antialising = 4;
    Vector3 total_col(0, 0, 0);
    // Vector3 col(255,255,255);
    for (int k = 0; k < antialising; k++)
    {
        Ray ray = cast_ray(i + float(k) / antialising, j + float(k) /antialising);
        
        Vector3 col(0, 0, 0);
        
        Vector3 rayOrigin = camera.get_pos();

        float d = RayMarch(rayOrigin, ray.get_dir());

        if (d < MAX_DIST)
            for (auto l : lights)
                col = compute_illumination(l, ray.get_dir(), ray.point_at(d));
        total_col = total_col + col;
    }
    return total_col / antialising;
}

void Scene::smoth()
{
    vector<Vector3> pixels_copy = vector<Vector3>(height * width, Vector3(0, 0, 0));
    for (int i = 1; i < width; i++)
        for (int j = 1; j < height; j++)
            pixels_copy[i + j * width] = pixels[i + j * width];

    for (int i = 1; i < width - 1; i++)
        for (int j = 1; j < height - 1; j++)
        {
            int a = i + 1 + j * width;
            int b = i - 1 + j * width;
            int c = i + (j - 1) * width;
            int d = i + (j + 1) * width;
            int e = i + 1 + (j + 1) * width;
            int f = i - 1 + (j - 1) * width;
            int g = i + 1 + (j - 1) * width;
            int h = i - 1 + (j + 1) * width;
            pixels[i + width * j] = pixels_copy[a] + pixels_copy[b] + pixels_copy[c] + pixels_copy[d] + pixels_copy[e] + pixels_copy[f] + pixels_copy[g] + pixels_copy[h];
            pixels[i + width * j] = pixels[i + width * j] / 8;
        }
}
