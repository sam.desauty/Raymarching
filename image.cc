#include "image.hh"

void Image::to_ppm(std::string filename)
{
    ofstream file(filename);
    file << "P3" << '\n';
    file << w << ' ' << h << '\n';
    file << 255 << '\n';
    for(size_t i = 0; i < w; i++)
    {
        for(size_t j = 0; j < h; j++)
        {
            file << m[i][j];
        }
        file << '\n';
    }
}