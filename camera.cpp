#include "camera.hh"

Camera::Camera(Vector3 pos, Vector3 watch_point, Vector3 top, float fov)
{
    this->pos = pos;
    this->watch_point = watch_point;
    this->fov = fov;
    this->up = top;
    third_dim = (watch_point-pos).normalize();
    right_vec = top.cross(third_dim); 
}

float Camera::get_fov(){
    return fov;
}

float Camera::get_distance(){
    return (watch_point-pos).distance();
}

Vector3 Camera::get_watch_point(){
    return watch_point;
}

Vector3 Camera::get_up(){
    return up;
}

Vector3 Camera::get_pos(){
    return pos;
}

Vector3 Camera::get_third_dim(){
    return third_dim;
}

Vector3 Camera::get_right_vec(){
    return right_vec;
}
