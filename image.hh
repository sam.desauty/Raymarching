#pragma once
#include "color.hh"
#include <string>
#include <fstream>

using namespace std;

struct Image
{
    Image(){}
    Image(size_t w, size_t h)
    :w(w), h(h)
    {
        m = new Color*[w];
        for (unsigned i = 0; i < w; i++)
            m[i] = new Color[h];
        aspect_ratio = (float) h / (float) w;
    }
    ~Image()
    {
        for (unsigned i = 0; i < w; i++)
        {
            delete[] m[i];
        }
        delete[] m;
    }
    void black_fill();
    void to_ppm(std::string filename);
    size_t w;
    size_t h;
    Color ** m;
    float aspect_ratio;
};
