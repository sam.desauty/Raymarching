
#pragma once
#include "vector3.hh"

class Camera{
    public:
        Camera(Vector3 pos, Vector3 watch_point, Vector3 top, float fov);

        float get_fov();
        float get_distance();

        Vector3 get_watch_point();
        Vector3 get_up();
        Vector3 get_pos();
        Vector3 get_third_dim();
        Vector3 get_right_vec();
    private:
        float fov;
        Vector3 pos;
        Vector3 watch_point;
        Vector3 up;
        Vector3 third_dim;
        Vector3 right_vec;
};