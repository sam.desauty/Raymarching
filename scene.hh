#pragma once

#include <vector>

#include "camera.hh"
#include "object.hh"
#include "light.hh"

class Scene
{
    public:

        Scene(Camera camera, vector<Object*> objects, vector<Light> lights,
            float width=0, float height=0);

        // static Scene parse_scene(string filename, Image *image);
        vector<Vector3> pixels;

        Camera get_camera();
        vector<Light> get_lights();
        float get_width();
        float get_height();
        
        void save_image(string name);
        Vector3 compute_pixel(float i, float j);
        Ray cast_ray(int i, int j);
        float get_dist(Vector3 p);
        Vector3 get_normal(Vector3 p);

        pair<Vector3, Uniform_Texture> get_color(Vector3 point);

        Vector3 compute_illumination(Light l, Vector3 r, Vector3 point, int count);

        void smoth();

        float RayMarch(Vector3 ro, Vector3 dir);

    private:
        Camera camera;
        vector<Object*> objects;
        vector<Light> lights;
        float width;
        float height;
};